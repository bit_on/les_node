'use strict';

var http = require('http');
var Menu = require('menu');
var Content = require('content');
var fs = require('fs');
var mimes = {
            '.jpg': { 'Content-Type' : 'image/jpeg'},
            '.js':{ 'Content-Type' : 'application/javascript'},
            '.css':{ 'Content-Type' : 'text/css'},
            '.html': { 'Content-Type' : 'text/html; charset=utf-8'}
          };

http.createServer(function (req,res) {

  var now = new Date();
  console.log('\n'+now);
  var reqUri = req.url;
  var menu = new Menu();
  var content = new Content();
  var dataContent = content.getContent(menu.activeItem(reqUri));
  var htmlTempl = '';
  var htmlMenu = '';
  var htmlContent = '';
  var title = dataContent.title;
  // 1st
  function getIt(cb) {
    fs.readFile('html_template/html_temp.html', (err, data, cb) =>{
      if (err) throw err;
      console.log('htmlTemplate in FSREAD :>' + data + '<');
      htmlTempl = '' + data;
      fs.readFile(dataContent.content, (err, data, cb)=>{
        if (err) throw err;
        console.log(data.toString);
        htmlContent = '' + data;
        console.log(htmlContent);
        fs.readFile('menu.html', (err, data, cb)=>{
          if (err) throw err;
          htmlMenu = '' + data;
          console.log(htmlMenu);
          function getIt(cb) {
            callback(cb);
            htmlTempl.replace("#{title}", title)
            .replace("#{menu}", htmlMenu)
            .replace("#{content}", htmlContent);
            console.log(htmlTempl);
            res.writeHead(200, mimes['.html']);
            console.log('ready');
            res.end(htmlTempl);
          };
        });
      });
    });
  };
  getIt(function () {
    console.log('ready');
  });


}).listen(8007);
